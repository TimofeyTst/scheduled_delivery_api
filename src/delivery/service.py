from sqlalchemy import delete, func, insert, select

from src.database import database, delivery, message
from src.delivery.constants import FAILED_STATUS, PENDING_STATUS, SENT_STATUS
from src.delivery.models import Delivery, DeliveryCreate, DeliveryInfo
from src.message.service import delete_message, read_messages_by_delivery_id


async def read_deliveries(skip: int = 0, limit: int = 100) -> list[Delivery]:
    select_query = select(delivery).offset(skip).limit(limit)
    return await database.fetch_all(select_query)


async def read_deliveries_detail(skip: int = 0, limit: int = 100) -> list[DeliveryInfo]:
    select_query = select(delivery).offset(skip).limit(limit)
    return await database.fetch_all(select_query)


async def read_deliveries_detail(skip: int = 0, limit: int = 100) -> list[DeliveryInfo]:
    delivery_message_count = func.count(message.c.id).label("total_message_count")
    pending_message_count = (
        func.count(message.c.id)
        .filter(message.c.status == PENDING_STATUS)
        .label("pending_message_count")
    )
    sent_message_count = (
        func.count(message.c.id)
        .filter(message.c.status == SENT_STATUS)
        .label("sent_message_count")
    )
    failed_message_count = (
        func.count(message.c.id)
        .filter(message.c.status == FAILED_STATUS)
        .label("failed_message_count")
    )

    select_query = (
        select(delivery)
        .outerjoin(message)
        .group_by(delivery.c.id)
        .offset(skip)
        .limit(limit)
        .add_columns(
            delivery_message_count,
            sent_message_count,
            failed_message_count,
            pending_message_count,
        )
    )
    return await database.fetch_all(select_query)


async def read_delivery_by_id(delivery_id: int) -> Delivery:
    select_query = select(delivery).where(delivery.c.id == delivery_id)
    return await database.fetch_one(select_query)


async def read_delivery_detail_by_id(delivery_id: int) -> DeliveryInfo:
    delivery_message_count = func.count(message.c.id).label("total_message_count")
    pending_message_count = (
        func.count(message.c.id)
        .filter(message.c.status == PENDING_STATUS)
        .label("pending_message_count")
    )
    sent_message_count = (
        func.count(message.c.id)
        .filter(message.c.status == SENT_STATUS)
        .label("sent_message_count")
    )
    failed_message_count = (
        func.count(message.c.id)
        .filter(message.c.status == FAILED_STATUS)
        .label("failed_message_count")
    )

    select_query = (
        select(delivery)
        .where(delivery.c.id == delivery_id)
        .outerjoin(message)
        .group_by(delivery.c.id)
        .add_columns(
            delivery_message_count,
            sent_message_count,
            failed_message_count,
            pending_message_count,
        )
    )

    return await database.fetch_one(select_query)


async def create_delivery(new_delivery: DeliveryCreate) -> Delivery:
    insert_query = insert(delivery).values(
        start=new_delivery.start,
        end=new_delivery.end,
        text=new_delivery.text,
        operator_code_filter=new_delivery.operator_code_filter,
        tag_filter=new_delivery.tag_filter,
    )
    delivery_id = await database.execute(insert_query)
    return await read_delivery_by_id(delivery_id)


async def delete_delivery(delivery_id: int):
    async with database.transaction():
        # Delete all messages with delivery_id
        messages = await read_messages_by_delivery_id(delivery_id)
        for message in messages:
            await delete_message(message.id)

        delete_delivery_query = delete(delivery).where(delivery.c.id == delivery_id)
        await database.execute(delete_delivery_query)
