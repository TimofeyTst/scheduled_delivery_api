from datetime import datetime

from sqlalchemy import delete, insert, select, update

from src.database import database, message
from src.message.models import Message, MessageCreate


async def read_messages(skip: int = 0, limit: int = 100) -> list[Message]:
    select_query = select(message).offset(skip).limit(limit)
    return await database.fetch_all(select_query)


async def read_messages_by_delivery_id(
    delivery_id: int, skip: int = 0, limit: int = 100
) -> list[Message]:
    select_query = (
        select(message)
        .where(message.c.delivery_id == delivery_id)
        .offset(skip)
        .limit(limit)
    )
    return await database.fetch_all(select_query)


async def read_messages_by_delivery_and_status(
    delivery_id: int, status: str, skip: int = 0, limit: int = 100
) -> list[Message]:
    select_query = (
        select(message)
        .where((message.c.delivery_id == delivery_id) & (message.c.status == status))
        .offset(skip)
        .limit(limit)
    )
    return await database.fetch_all(select_query)


async def read_message_by_id(message_id: int) -> Message:
    select_query = select(message).where(message.c.id == message_id)
    return await database.fetch_one(select_query)


async def create_message(new_message: MessageCreate) -> Message:
    insert_query = insert(message).values(
        created=datetime.utcnow(),
        status=new_message.status,
        delivery_id=new_message.delivery_id,
        client_id=new_message.client_id,
    )
    message_id = await database.execute(insert_query)
    return await read_message_by_id(message_id)


async def update_message(message_id: int, updated_message: MessageCreate) -> Message:
    update_query = (
        update(message)
        .where(message.c.id == message_id)
        .values(
            status=updated_message.status,
            delivery_id=updated_message.delivery_id,
            client_id=updated_message.client_id,
        )
    )
    await database.execute(update_query)
    return await read_message_by_id(message_id)


async def update_message_status(message_id: int, message_status: str) -> Message:
    update_query = (
        update(message)
        .where(message.c.id == message_id)
        .values(
            status=message_status,
        )
    )
    await database.execute(update_query)
    return await read_message_by_id(message_id)


async def delete_message(message_id: int):
    delete_message_query = delete(message).where(message.c.id == message_id)
    await database.execute(delete_message_query)
