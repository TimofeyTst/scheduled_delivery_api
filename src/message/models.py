from datetime import datetime

from pydantic import BaseModel

from src.client.models import Client


class MessageBase(BaseModel):
    status: str
    delivery_id: int
    client_id: int


class MessageCreate(MessageBase):
    pass


class Message(MessageBase):
    id: int
    created: datetime

    class Config:
        orm_mode = True


class MessageDetailInfo(Message):
    client: Client
