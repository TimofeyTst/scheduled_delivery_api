import pytest
from async_asgi_testclient import TestClient
from fastapi import status

@pytest.mark.asyncio
async def test_create_client(client: TestClient) -> None:
    json_data = {
            "phone_number": "79054125050",
            "operator_code": "7962",
            "tag": "VIP",
            "timezone": "Europe/Moscow"
        }
    resp = await client.post(
        "/client",
        json=json_data,
    )
    resp_json = resp.json()

    assert resp.status_code == status.HTTP_201_CREATED
    resp_json.pop("id", None)
    assert resp_json == json_data



@pytest.mark.asyncio
async def test_create_and_get(client: TestClient) -> None:
    json_data = {
            "phone_number": "79054135050",
            "operator_code": "7962",
            "tag": "VIP",
            "timezone": "Europe/Moscow"
        }
    resp = await client.post(
        "/client",
        json=json_data,
    )
    resp_json = resp.json()

    get_resp = await client.get(f"/client/{resp_json['id']}")
    assert get_resp.status_code == status.HTTP_200_OK
    retrieved_data = get_resp.json()

    # Проверяем, что данные из ответа на GET-запрос совпадают с данными, отправленными в POST-запросе
    assert retrieved_data == resp_json