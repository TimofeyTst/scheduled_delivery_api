from datetime import datetime
from typing import Optional

from pydantic import BaseModel

from src.message.models import MessageDetailInfo


class DeliveryBase(BaseModel):
    start: datetime
    end: datetime
    operator_code_filter: Optional[str]
    tag_filter: Optional[str]
    text: str


class DeliveryCreate(DeliveryBase):
    pass


class Delivery(DeliveryBase):
    id: int

    class Config:
        orm_mode = True


class DeliveryMessage(BaseModel):
    id: int
    message: str


class DeliveryInfo(Delivery):
    total_message_count: int
    pending_message_count: int
    sent_message_count: int
    failed_message_count: int


class DeliveryDetailInfo(DeliveryInfo):
    sent_messages: list[MessageDetailInfo]
    failed_messages: list[MessageDetailInfo]
    pending_messages: list[MessageDetailInfo]
