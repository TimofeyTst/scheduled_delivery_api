from fastapi import APIRouter, HTTPException

from src.client.service import read_client_by_id
from src.message.models import Message, MessageDetailInfo
from src.message.service import read_message_by_id, read_messages
from src.message.utils import get_detailed_messages

router = APIRouter()


@router.get("/messages", response_model=list[Message])
async def get_messages(skip: int = 0, limit: int = 100):
    messages = await read_messages(skip=skip, limit=limit)
    return messages


@router.get("/messages/detail", response_model=list[MessageDetailInfo])
async def get_messages_detail(skip: int = 0, limit: int = 100):
    messages = await read_messages(skip=skip, limit=limit)
    return await get_detailed_messages(messages)


@router.get("/message/{message_id}", response_model=Message)
async def get_message_by_id(message_id: int):
    message = await read_message_by_id(message_id)
    if message:
        return message
    else:
        raise HTTPException(status_code=404, detail="Message not found")


@router.get("/message/{message_id}/detail", response_model=MessageDetailInfo)
async def get_message_detail_by_id(message_id: int):
    message = await read_message_by_id(message_id)
    client = await read_client_by_id(message.client_id)
    if message:
        return MessageDetailInfo(**message, client=client)
    else:
        raise HTTPException(status_code=404, detail="Message not found")
