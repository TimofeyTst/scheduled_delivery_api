from databases import Database
from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Integer,
    MetaData,
    String,
    Table,
    create_engine,
    func,
)

from src.config import settings
from src.constants import DB_NAMING_CONVENTION

DATABASE_URL = settings.DATABASE_URL
engine = create_engine(DATABASE_URL)

metadata = MetaData(naming_convention=DB_NAMING_CONVENTION)

database = Database(DATABASE_URL, force_rollback=settings.ENVIRONMENT.is_testing)

delivery = Table(
    "delivery",
    metadata,
    Column("id", Integer, primary_key=True, index=True),
    Column("start", DateTime(timezone=True)),
    Column("end", DateTime(timezone=True)),
    Column("text", String),
    Column("operator_code_filter", String),
    Column("tag_filter", String),
)

client = Table(
    "client",
    metadata,
    Column("id", Integer, primary_key=True, index=True),
    Column("phone_number", String, unique=True),
    Column("operator_code", String),
    Column("tag", String),
    Column("timezone", String),
)

message = Table(
    "message",
    metadata,
    Column("id", Integer, primary_key=True, index=True),
    Column(
        "created", DateTime(timezone=True), server_default=func.now(), nullable=False
    ),
    Column("status", String),
    Column("delivery_id", Integer, ForeignKey("delivery.id")),
    Column("client_id", Integer, ForeignKey("client.id")),
)

# # Добавим связи между таблицами

# delivery_messages = relationship(
#     "message", back_populates="delivery", cascade="all, delete-orphan"
# )
# client_messages = relationship(
#     "message", back_populates="client", cascade="all, delete-orphan"
# )

# # А теперь привяжем связи к созданным таблицам

# message.c.delivery = relationship(
#     "delivery",
#     back_populates="delivery_messages",
#     primaryjoin=message.c.delivery_id == delivery.c.id,
# )

# message.c.client = relationship(
#     "client",
#     back_populates="client_messages",
#     primaryjoin=message.c.client_id == client.c.id,
# )
