import asyncio
from datetime import datetime

import aiohttp

from src.client.models import Client
from src.config import settings
from src.delivery.constants import FAILED_STATUS, PENDING_STATUS, SENT_STATUS
from src.message.models import MessageCreate
from src.message.service import create_message, update_message_status


class DeliveryManager:
    def __init__(self):
        self.__delivery_tasks = {}

    def get_active_delivery_ids(self):
        active_delivery_ids = []
        for delivery_id, delivery_task in self.__delivery_tasks.items():
            if not delivery_task.done():
                active_delivery_ids.append(delivery_id)
        return active_delivery_ids

    def get_delivery_task(self, delivery_id: int):
        return self.__delivery_tasks[delivery_id]

    def add_delivery_task(self, delivery_id: int):
        self.__delivery_tasks[delivery_id] = asyncio.current_task()

    def del_delivery_task(self, delivery_id: int):
        del self.__delivery_tasks[delivery_id]

    def is_delivery_tasks_contains(self, delivery_id: int) -> bool:
        return delivery_id in self.__delivery_tasks

    async def immediatly_delivery(
        self, delivery_id: int, text: str, clients: list[Client], end_time: datetime
    ):
        self.add_delivery_task(delivery_id)
        await self.start_delivery(delivery_id, text, clients, end_time)

    async def schedule_delivery(
        self,
        delay_seconds: float,
        delivery_id: int,
        text: str,
        clients: list[Client],
        end_time: datetime,
    ):
        try:
            self.add_delivery_task(delivery_id)
            await asyncio.sleep(delay_seconds)
            await self.start_delivery(delivery_id, text, clients, end_time)
        except asyncio.CancelledError:
            print(f"Delivery task with id = {delivery_id} was cancelled")

    async def start_delivery(
        self, delivery_id: int, text: str, clients: list[Client], end_time: datetime
    ):
        print(
            f"self.scheduled_deliveries {self.__delivery_tasks} asyncio {asyncio.current_task()}"
        )
        print("Starting delivery")

        for client in clients:
            now_with_timezone = datetime.now().astimezone(end_time.tzinfo)
            if now_with_timezone >= end_time:
                print(
                    f"Delivery execution time exceeded end time. Cancelling delivery with id {delivery_id}"
                )
                break

            created_message = await create_message(
                MessageCreate(
                    status=PENDING_STATUS, delivery_id=delivery_id, client_id=client.id
                )
            )
            # Try to send the message and update the message status accordingly
            if await self.send_message(created_message.id, client.phone_number, text):
                await update_message_status(created_message.id, SENT_STATUS)
            else:
                await update_message_status(created_message.id, FAILED_STATUS)

        self.del_delivery_task(delivery_id)

    async def send_message(self, msg_id: int, phone: str, text: str):
        try:
            # Construct the request URL using settings.API_URL and msg_id
            url = f"{settings.API_URL}{msg_id}"

            # Prepare the request headers with the JWT access token
            headers = {
                "Authorization": f"Bearer {settings.ACCESS_TOKEN}",
                "Content-Type": "application/json",
            }

            # Prepare the request body with the phone number and text
            data = {"id": msg_id, "phone": phone, "text": text}
            print(f"Try to sending message url={url} data={data}")

            # Send the POST request using aiohttp
            async with aiohttp.ClientSession() as session:
                async with session.post(url, json=data, headers=headers) as response:
                    if response.status == 200:
                        response_data = await response.json()
                        if response_data.get("message") == "OK":
                            return True
            return False

        except Exception:
            # Handle any exceptions that may occur during the request
            return False

    def cancel_delivery(self, delivery_id: int) -> bool:
        if self.is_delivery_tasks_contains(delivery_id):
            delivery_task = self.get_delivery_task(delivery_id)
            if not delivery_task.done():
                delivery_task.cancel()
                self.del_delivery_task(delivery_id)
                return True
        return False


# Использование класса DeliveryManager:
delivery_manager = DeliveryManager()
