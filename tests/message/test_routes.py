import pytest
from async_asgi_testclient import TestClient
from fastapi import status

@pytest.mark.asyncio
async def test_get_messages(client: TestClient) -> None:
    resp = await client.get("/messages")
    assert resp.status_code == status.HTTP_200_OK


@pytest.mark.asyncio
async def test_get_messages_detail(client: TestClient) -> None:
    resp = await client.get("/messages/detail")
    assert resp.status_code == status.HTTP_200_OK


@pytest.mark.asyncio
async def test_get_message_by_id(client: TestClient, setup_db) -> None:
    clients, deliveries, messages = await setup_db

    resp = await client.get(f"/message/{messages[0].id}")
    assert resp.status_code == status.HTTP_200_OK


@pytest.mark.asyncio
async def test_get_message_detail_by_id(client: TestClient, setup_db) -> None:
    clients, deliveries, messages = await setup_db

    resp = await client.get(f"/message/{messages[0].id}/detail")
    assert resp.status_code == status.HTTP_200_OK

