from sqlalchemy import delete, insert, select, update

from src.client.models import Client, ClientCreate
from src.database import client, database, message


async def read_clients(skip: int = 0, limit: int = 100) -> list[Client]:
    select_query = select(client).offset(skip).limit(limit)
    return await database.fetch_all(select_query)


async def read_client_by_id(client_id: int) -> Client:
    select_query = select(client).where(client.c.id == client_id)
    return await database.fetch_one(select_query)


async def create_client(new_client: ClientCreate) -> Client:
    insert_query = (
        insert(client)
        .values(
            phone_number=new_client.phone_number,
            operator_code=new_client.operator_code,
            tag=new_client.tag,
            timezone=new_client.timezone,
        )
        .returning(client)
    )

    return await database.fetch_one(insert_query)


async def update_client(client_id: int, updated_client: ClientCreate) -> Client:
    update_query = (
        update(client)
        .where(client.c.id == client_id)
        .values(
            phone_number=updated_client.phone_number,
            operator_code=updated_client.operator_code,
            tag=updated_client.tag,
            timezone=updated_client.timezone,
        )
    )
    await database.execute(update_query)
    return await read_client_by_id(client_id)


async def delete_client(client_id: int):
    async with database.transaction():
        # Delete all messages with client_id
        delete_messages_query = delete(message).where(message.c.client_id == client_id)
        await database.execute(delete_messages_query)

        delete_client_query = delete(client).where(client.c.id == client_id)
        return await database.execute(delete_client_query)


async def read_filtered_clients(operator_code: str, tag: str) -> list[Client]:
    select_query = select(client).where(
        (client.c.operator_code == operator_code) & (client.c.tag == tag)
    )
    clients = await database.fetch_all(select_query)
    return clients
