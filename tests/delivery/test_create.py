import pytest
from async_asgi_testclient import TestClient
from fastapi import status


@pytest.mark.asyncio
async def test_create_immediately_delivery(client: TestClient, setup_db, setup_time_now) -> None:
    await setup_db
    start_date_str, end_date_str = setup_time_now

    json_data = {
        "start": start_date_str,
        "end": end_date_str,
        "operator_code_filter": "7962",
        "tag_filter": "VIP",
        "text": "Test text"
    }
    resp = await client.post("/delivery", json=json_data)
    resp_json = resp.json()

    assert resp.status_code == status.HTTP_201_CREATED
    assert resp_json['message'] == "Delivery created immediately"


@pytest.mark.asyncio
async def test_create_schedule_delivery(client: TestClient, setup_db, setup_time) -> None:
    await setup_db
    start_date_str, end_date_str = setup_time

    json_data = {
        "start": start_date_str,
        "end": end_date_str,
        "operator_code_filter": "7962",
        "tag_filter": "VIP",
        "text": "Test text"
    }
    resp = await client.post("/delivery", json=json_data)
    resp_json = resp.json()

    assert resp.status_code == status.HTTP_201_CREATED
    assert resp_json['message'].startswith("Delivery will start in")


@pytest.mark.asyncio
async def test_create_empty_clients_delivery(client: TestClient, setup_db, setup_time) -> None:
    await setup_db
    start_date_str, end_date_str = setup_time

    json_data = {
        "start": start_date_str,
        "end": end_date_str,
        "operator_code_filter": "7962",
        "tag_filter": "AMAZING NONE EXIST FILTER",
        "text": "Test text"
    }
    resp = await client.post("/delivery", json=json_data)
    resp_json = resp.json()

    assert resp.status_code == status.HTTP_201_CREATED
    assert resp_json['message'] == "Empty clients list"