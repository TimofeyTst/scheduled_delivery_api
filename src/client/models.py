from pydantic import BaseModel


class ClientBase(BaseModel):
    phone_number: str
    operator_code: str
    tag: str
    timezone: str


class ClientCreate(ClientBase):
    pass


class Client(ClientBase):
    id: int
    phone_number: str
    operator_code: str
    tag: str
    timezone: str

    class Config:
        orm_mode = True
