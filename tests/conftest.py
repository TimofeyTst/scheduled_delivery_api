import asyncio
from typing import Any, Generator, AsyncGenerator

import pytest
import pytest_asyncio
from async_asgi_testclient import TestClient

from src.client.models import ClientCreate
from src.client.service import create_client
from src.delivery.models import DeliveryCreate
from src.delivery.service import create_delivery
from src.message.models import MessageCreate
from src.message.service import create_message

from src.main import app


@pytest.fixture(autouse=True, scope="session")
def run_migrations() -> None:
    import os

    print("running migrations..")
    os.system("alembic upgrade head")


@pytest.fixture(scope="session")
def event_loop() -> Generator[asyncio.AbstractEventLoop, None, None]:
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest_asyncio.fixture
async def client() -> AsyncGenerator[TestClient, None]:
    host, port = "127.0.0.1", "8000"
    scope = {"client": (host, port)}

    async with TestClient(app, scope=scope) as client:
        yield client


@pytest.fixture
async def setup_db():
    clients = [
        ClientCreate(
            phone_number="79014125050",
            operator_code="7962",
            tag="VIP",
            timezone="Europe/Moscow"
        ),
        ClientCreate(
            phone_number="79123456789",
            operator_code="7962",
            tag="VIP",
            timezone="Europe/Moscow"
        )
    ]

    created_clients = []
    for client in clients:
        created_client = await create_client(client)
        created_clients.append(created_client)

    deliveries = [
        DeliveryCreate(
            start="2023-07-21T14:23:17.077000+00:00",
            end="2023-07-21T14:25:17.077000+00:00",
            operator_code_filter="7962",
            tag_filter= "VIP",
            text="TST",
        ),
        DeliveryCreate(
            start="2023-07-22T11:23:17.077000+00:00",
            end="2023-07-22T14:23:17.077000+00:00",
            operator_code_filter="7962",
            tag_filter= "VIP",
            text="TST",
        ),
    ]

    created_deliveries = []
    for delivery in deliveries:
        created_delivery = await create_delivery(delivery)
        created_deliveries.append(created_delivery)

    messages = [
        MessageCreate(
            status="Sent",
            delivery_id=created_deliveries[0].id,
            client_id=created_clients[0].id,
            created="2023-07-21T14:23:18.077000+00:00"
        ),
        MessageCreate(
            status="Sent",
            delivery_id=created_deliveries[0].id,
            client_id=created_clients[1].id,
            created="2023-07-21T14:23:19.077000+00:00"
        ),
        MessageCreate(
            status="Sent",
            delivery_id=created_deliveries[1].id,
            client_id=created_clients[0].id,
            created="2023-07-22T11:24:17.077000+00:00"
        ),
        MessageCreate(
            status="Failed",
            delivery_id=created_deliveries[1].id,
            client_id=created_clients[1].id,
            created="2023-07-22T11:25:17.077000+00:00"
        ),
    ]

    created_messages = []
    for message in messages:
        created_message = await create_message(message)
        created_messages.append(created_message)

    return created_clients, created_deliveries, created_messages