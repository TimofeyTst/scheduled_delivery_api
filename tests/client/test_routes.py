import pytest
from async_asgi_testclient import TestClient
from fastapi import status

@pytest.mark.asyncio
async def test_get_clients(client: TestClient, setup_db) -> None:
    await setup_db
    resp = await client.get("/clients")
    assert resp.status_code == status.HTTP_200_OK


@pytest.mark.asyncio
async def test_get_client_by_id(client: TestClient, setup_db) -> None:
    clients, deliveries, messages = await setup_db

    resp = await client.get(f"/client/{clients[0].id}")
    assert resp.status_code == status.HTTP_200_OK


@pytest.mark.asyncio
async def test_update_client_by_id(client: TestClient, setup_db) -> None:
    clients, deliveries, messages = await setup_db
    json_data = {
        "phone_number": "79991232215",
        "operator_code": "7999",
        "tag": "Verification",
        "timezone": "Europe/Moscow"
    }
    resp = await client.put(f"/client/{clients[0].id}", json=json_data)
    assert resp.status_code == status.HTTP_200_OK
    resp_json = resp.json()

    resp_json.pop("id", None)
    assert resp_json == json_data


@pytest.mark.asyncio
async def test_delete_client_by_id(client: TestClient, setup_db) -> None:
    clients, deliveries, messages = await setup_db
    resp = await client.delete(f"/client/{clients[0].id}")
    assert resp.status_code == status.HTTP_200_OK
    
    data = resp.json()
    assert data == {"message": "Client deleted successfully"} 