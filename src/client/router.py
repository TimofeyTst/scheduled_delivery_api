from asyncpg.exceptions import UniqueViolationError
from fastapi import APIRouter, HTTPException, status

from src.client.models import Client, ClientCreate
from src.client.service import (
    create_client,
    delete_client,
    read_client_by_id,
    read_clients,
    update_client,
)

router = APIRouter()


@router.get("/clients", response_model=list[Client])
async def get_clients(skip: int = 0, limit: int = 100):
    clients = await read_clients(skip=skip, limit=limit)
    return clients


@router.get("/client/{client_id}", response_model=Client)
async def get_client_by_id(client_id: int):
    client = await read_client_by_id(client_id)
    if client:
        return client
    else:
        raise HTTPException(status_code=404, detail="Client not found")


@router.post("/client", status_code=status.HTTP_201_CREATED, response_model=Client)
async def create_new_client(client: ClientCreate):
    try:
        created_client = await create_client(client)
        return created_client
    except UniqueViolationError as e:
        raise HTTPException(status_code=400, detail=e.message)


@router.put("/client/{client_id}", response_model=Client)
async def update_existing_client(client_id: int, new_client: ClientCreate):
    exist_client = await read_client_by_id(client_id)
    if exist_client:
        return await update_client(client_id, new_client)
    else:
        raise HTTPException(status_code=404, detail="Client not found")


@router.delete("/client/{client_id}")
async def delete_existing_client(client_id: int):
    exist_client = await read_client_by_id(client_id)
    if exist_client:
        await delete_client(client_id)
        return {"message": "Client deleted successfully"}
    else:
        raise HTTPException(status_code=404, detail="Client not found")
