from datetime import datetime, timedelta, timezone

import pytest

@pytest.fixture
def setup_time_now(end_ms: int = 300):
    start_datetime = datetime.now(timezone.utc)
    start_date_str = start_datetime.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    end_datetime = start_datetime + timedelta(milliseconds=end_ms)
    end_date_str = end_datetime.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

    return start_date_str, end_date_str


@pytest.fixture
def setup_time(start_ms: int = 100, end_ms: int = 300):
    start_datetime = datetime.now(timezone.utc) + timedelta(milliseconds=start_ms)
    start_date_str = start_datetime.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    end_datetime = start_datetime + timedelta(milliseconds=end_ms)
    end_date_str = end_datetime.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

    return start_date_str, end_date_str