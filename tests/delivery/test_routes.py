import pytest
from async_asgi_testclient import TestClient
from fastapi import status

@pytest.mark.asyncio
async def test_get_deliveries(client: TestClient) -> None:
    resp = await client.get("/deliveries")
    assert resp.status_code == status.HTTP_200_OK


@pytest.mark.asyncio
async def test_get_deliveries_detail(client: TestClient) -> None:
    resp = await client.get("/deliveries/detail")
    assert resp.status_code == status.HTTP_200_OK


@pytest.mark.asyncio
async def test_get_delivery_by_id(client: TestClient, setup_db) -> None:
    clients, deliveries, messages = await setup_db
    resp = await client.get(f"/delivery/{deliveries[0].id}")
    assert resp.status_code == status.HTTP_200_OK
    
    data = resp.json()
    assert data['operator_code_filter'] == deliveries[0].operator_code_filter 
    assert data['tag_filter'] == deliveries[0].tag_filter 
    assert data['text'] == deliveries[0].text 
    assert data['id'] == deliveries[0].id 


@pytest.mark.asyncio
async def test_delete_delivery_by_id(client: TestClient, setup_db) -> None:
    clients, deliveries, messages = await setup_db
    resp = await client.delete(f"/delivery/{deliveries[0].id}")
    assert resp.status_code == status.HTTP_200_OK
    
    data = resp.json()
    assert data == {"message": "Delivery deleted successfully"} 


@pytest.mark.asyncio
async def test_get_delivery_detail_by_id(client: TestClient, setup_db) -> None:
    clients, deliveries, messages = await setup_db
    resp = await client.get(f"/delivery/{deliveries[0].id}/detail")
    assert resp.status_code == status.HTTP_200_OK

        
    data = resp.json()
    assert data['sent_message_count'] == 2 
    assert data['operator_code_filter'] == deliveries[0]['operator_code_filter'] 
    assert data['tag_filter'] == deliveries[0].tag_filter 
    assert data['text'] == deliveries[0].text 
    assert data['id'] == deliveries[0].id

    