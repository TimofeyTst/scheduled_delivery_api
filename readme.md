# Сервис управления рассылками
## Настройка из докера
Предварительно нужно создать файл ```.env``` в корне проекта по шаблону .env.example:
```
DATABASE_URL=postgresql://POSTGRES_USER:PASSWORD@scheduled_delivery_db:5432/scheduled

SITE_DOMAIN=0.0.0.0
SECURE_COOKIES=false

ENVIRONMENT=LOCAL

CORS_HEADERS=["*"]
CORS_ORIGINS=["http://localhost:3000"]

API_URL="https://example.com/send/msgId"
ACCESS_TOKEN="TOKEN"

```
Далее надо освободить порт 5432, используемый обычно Postgres, после чего запускаем контейнер:

```shell
docker network create scheduled_main
docker-compose up -d --build
```
### Linter
```shell
docker compose exec scheduled_delivery_api format
```

### Tests
```shell
docker compose exec scheduled_delivery_api pytest
```

### Migrations
- Create an automatic migration from changes in `src/database.py`
```shell
docker compose exec scheduled_delivery_api makemigrations *migration_name*
```
docker compose exec scheduled_delivery_api makemigrations create_init

- Run migrations
```shell
docker compose exec scheduled_delivery_api migrate
```
- Downgrade migrations
```shell
docker compose exec scheduled_delivery_api downgrade -1  # or -2 or base or hash of the migration
```
------
## Работа с запущенным сервером
По умолчанию сервер доступен на локальной сети на порту 8000
http://127.0.0.1:8000/
> Для просмотра документации допишите в конце адреса путь docs:
> http://127.0.0.1:8000/docs

## Production deploy
В файл .env добавить переменную SENTRY_DSN для мониторинга и заменить ENVIRONMENT. Например: 

```
...
ENVIRONMENT=PRODUCTION

SENTRY_DSN=https://123456789.ingest.sentry.io/987654321
...
```

Запустить контейнер
```shell
docker network create scheduled_main
docker-compose -f docker-compose.prod.yml up -d --build
```


