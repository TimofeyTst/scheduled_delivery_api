from datetime import datetime

from fastapi import APIRouter, BackgroundTasks, HTTPException, status

from src.client.service import read_filtered_clients
from src.delivery.delivery_manager import delivery_manager
from src.delivery.models import (
    Delivery,
    DeliveryCreate,
    DeliveryDetailInfo,
    DeliveryInfo,
    DeliveryMessage,
)
from src.delivery.service import (
    create_delivery,
    delete_delivery,
    read_deliveries,
    read_deliveries_detail,
    read_delivery_by_id,
    read_delivery_detail_by_id,
)
from src.delivery.utils import get_detailed_deliveries, get_detailed_delivery

router = APIRouter()


@router.get("/deliveries", response_model=list[Delivery])
async def get_deliveries(skip: int = 0, limit: int = 100):
    deliveries = await read_deliveries(skip=skip, limit=limit)
    return deliveries


@router.get("/deliveries/detail", response_model=list[DeliveryInfo])
async def get_deliveries_detail(skip: int = 0, limit: int = 100):
    deliveries = await read_deliveries_detail(skip=skip, limit=limit)
    return deliveries


@router.get("/deliveries/active", response_model=list[DeliveryDetailInfo])
async def get_active_deliveries():
    delivery_ids = delivery_manager.get_active_delivery_ids()
    deliveries = []
    for delivery_id in delivery_ids:
        deliveries.append(await read_delivery_detail_by_id(delivery_id))

    if deliveries:
        return await get_detailed_deliveries(deliveries)
    else:
        raise HTTPException(status_code=404, detail="No active deliveries found")


@router.get("/delivery/{delivery_id}", response_model=Delivery)
async def get_delivery_by_id(delivery_id: int):
    delivery = await read_delivery_by_id(delivery_id)
    if delivery:
        return delivery
    else:
        raise HTTPException(status_code=404, detail="Delivery not found")


@router.get("/delivery/{delivery_id}/detail", response_model=DeliveryDetailInfo)
async def get_delivery_detail_by_id(delivery_id: int):
    delivery = await read_delivery_detail_by_id(delivery_id)
    if delivery:
        return await get_detailed_delivery(delivery)
    else:
        raise HTTPException(status_code=404, detail="Delivery not found")


@router.post(
    "/delivery", status_code=status.HTTP_201_CREATED, response_model=DeliveryMessage
)
async def create_new_delivery(
    delivery: DeliveryCreate,
    background_tasks: BackgroundTasks,
):
    created_delivery = await create_delivery(delivery)
    clients = await read_filtered_clients(
        created_delivery.operator_code_filter, created_delivery.tag_filter
    )
    now_with_timezone = datetime.now().astimezone(delivery.start.tzinfo)

    if not clients:
        return DeliveryMessage(id=created_delivery.id, message="Empty clients list")

    if delivery.start > delivery.end:
        raise HTTPException(status_code=400, detail="End time less than start time")

    if delivery.start <= now_with_timezone < delivery.end:
        background_tasks.add_task(
            delivery_manager.immediatly_delivery,
            created_delivery.id,
            created_delivery.text,
            clients,
            created_delivery.end,
        )
        return DeliveryMessage(
            id=created_delivery.id, message="Delivery created immediately"
        )
    elif delivery.start > now_with_timezone:
        # Schedule the delivery for automatic start at the specified time
        time_until_start = (delivery.start - now_with_timezone).total_seconds()
        background_tasks.add_task(
            delivery_manager.schedule_delivery,
            time_until_start,
            created_delivery.id,
            created_delivery.text,
            clients,
            created_delivery.end,
        )
        return DeliveryMessage(
            id=created_delivery.id,
            message=f"Delivery will start in {time_until_start} seconds",
        )
    else:
        raise HTTPException(status_code=400, detail="End time less than current time")


@router.put("/delivery/{delivery_id}/stop")
async def stop_delivery(delivery_id: int):
    delivery = await read_delivery_by_id(delivery_id)
    if not delivery:
        raise HTTPException(status_code=404, detail="Delivery not found")

    response = delivery_manager.cancel_delivery(delivery_id)
    if response:
        return {"message": "Scheduled delivery has been cancelled"}
    else:
        raise HTTPException(status_code=404, detail="Active delivery not found")


@router.delete("/delivery/{delivery_id}")
async def delete_existing_delivery(delivery_id: int):
    exist_delivery = await read_delivery_by_id(delivery_id)
    if exist_delivery:
        response = delivery_manager.cancel_delivery(delivery_id)
        await delete_delivery(delivery_id)
        if response:
            return {"message": "Delivery deleted and stopped successfully"}
        return {"message": "Delivery deleted successfully"}
    else:
        raise HTTPException(status_code=404, detail="Delivery not found")
