from src.client.service import read_client_by_id
from src.message.models import Message, MessageDetailInfo


async def get_detailed_messages(messages: list[Message]) -> list[MessageDetailInfo]:
    messages_detail = []
    for message in messages:
        client = await read_client_by_id(message.client_id)
        message_detail = MessageDetailInfo(**message, client=client)
        messages_detail.append(message_detail)

    return messages_detail
