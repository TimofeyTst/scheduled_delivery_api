from src.delivery.constants import FAILED_STATUS, PENDING_STATUS, SENT_STATUS
from src.delivery.models import Delivery, DeliveryDetailInfo
from src.message.service import read_messages_by_delivery_and_status
from src.message.utils import get_detailed_messages


async def get_detailed_delivery(delivery: Delivery) -> list[DeliveryDetailInfo]:
    pending_messages = await read_messages_by_delivery_and_status(
        delivery.id, status=PENDING_STATUS
    )
    sent_messages = await read_messages_by_delivery_and_status(
        delivery.id, status=SENT_STATUS
    )
    failed_messages = await read_messages_by_delivery_and_status(
        delivery.id, status=FAILED_STATUS
    )

    pending_messages_detailed = await get_detailed_messages(pending_messages)
    sent_messages_detailed = await get_detailed_messages(sent_messages)
    failed_messages_detailed = await get_detailed_messages(failed_messages)

    return DeliveryDetailInfo(
        **delivery,
        sent_messages=sent_messages_detailed,
        pending_messages=pending_messages_detailed,
        failed_messages=failed_messages_detailed
    )


async def get_detailed_deliveries(deliveries: Delivery) -> list[DeliveryDetailInfo]:
    detailed_deliveries = []
    for delivery in deliveries:
        detailed_deliveries.append(await get_detailed_delivery(delivery))

    return detailed_deliveries
